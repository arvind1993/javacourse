package com.day3;

public class Account {

	public static void main(String[] args) {
		double balance= 500.00;
		double amount= 1.00;
		
		if (amount<=0)
		{
			System.out.println("cannot withdraw negative amount");
		}
		
		else if (amount>balance)
		{	
				System.out.println("cannot withdraw money insufficient balance");
		}
		else
		{
			balance=balance-amount;
			System.out.println("Withdraw successful");
			System.out.println("The current balance is "+balance);
		}

	}

}
