package com.day3.ifelse;

import java.util.Scanner;

public class Account {

	public static void main(String[] args) {
		double balance=500.00;
		double amount;
		System.out.println("Amount to withdraw");
		Scanner s=new Scanner(System.in);
		amount=s.nextDouble();
		if(amount<=0 || amount>balance)
		{
			System.out.println("Withdrawal has failed");
		}
		else
		{
			balance-=amount;
			System.out.println("Withdrawal has succeeded");
			System.out.println("Current balance is "+balance);
		}
		s.close();	

	}

}
