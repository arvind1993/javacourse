package com.day3.dowhileloop;

public class Account {

	public static void main(String[] args) {
		double balance = 0,minbal = 500, depositAmt = 100;
		
		do 
		{
		System.out.println("$100 has been added to your account"); //100 200 
		depositAmt += 100;
		}
		
		while(depositAmt < minbal);
		balance = depositAmt;
		System.out.println("Transaction complete");
		System.out.println("My balance is "+balance);
		
			
		}

	}



