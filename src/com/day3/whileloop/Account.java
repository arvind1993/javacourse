package com.day3.whileloop;

import java.util.Scanner;

public class Account {

	public static void main(String[] args) {
		double balance=0,depositAmt=0,minBal=500;
		Scanner sc=new Scanner(System.in);
		while(depositAmt<minBal)
		{
			System.out.println(" Amount deposited in your account is "+depositAmt);
			depositAmt=sc.nextInt();
			//depositAmt+=depositAmt;
			
		}
		sc.close();
		System.out.println("Transaction complete");
		System.out.println("My current balance is "+balance);

	}

}
