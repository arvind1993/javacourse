package com.day5;

public class Bank {

	private String bankName,area;
	private String phoneNumber;
	
	Bank()
	{
		bankName="ICICI";
		area="Tilak nagar";
		phoneNumber="9908767881";	
	}

	
	public Bank(String bankName, String area, String phoneNumber) {
		super();
		this.bankName = bankName;
		this.area = area;
		this.phoneNumber = phoneNumber;
	}


	public String getBankName() {
		return bankName;
	}

	public String getArea() {
		return area;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}
	

}
