package com.day1;

public class Runner {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Employee emp1=new Employee();
		Employee emp2=new Employee();
		Employee emp3=new Employee();
		Account a1= new Account();
		
		emp1.setEmpid("emp101");
		emp1.setName("Arvind");
		emp1.setAddress("Mumbai");
		emp1.setMobileno("903294234");
		emp1.setDesignation("QA");
		
		System.out.println("=================Employee 1 details======================");
		System.out.println(emp1.getEmpid());
		System.out.println(emp1.getName());
		System.out.println(emp1.getAddress());
		System.out.println(emp1.getMobileno());
		System.out.println(emp1.getDesignation());
		
		emp2.setEmpid("emp102");
		emp2.setName("Ashwin");
		emp2.setAddress("Pune");
		emp2.setMobileno("987294234");
		emp2.setDesignation("SDET");
		
		System.out.println("================Employee 2 details==================");
		
		System.out.println(emp2.getEmpid());
		System.out.println(emp2.getName());
		System.out.println(emp2.getAddress());
		System.out.println(emp2.getMobileno());
		System.out.println(emp2.getDesignation());
		
		
		
		emp3.setEmpid("emp103");
		emp3.setName("Jatin");
		emp3.setAddress("Hyderabad");
		emp3.setMobileno("903294569");
		emp3.setDesignation("QA Lead");
		
		System.out.println("===============Employee 3 details==================");
		System.out.println(emp3.getEmpid());
		System.out.println(emp3.getName());
		System.out.println(emp3.getAddress());
		System.out.println(emp3.getMobileno());
		System.out.println(emp3.getDesignation());
		
		
		System.out.println("==================Account details=================");
		a1.setCreditamt(10000);
		a1.setDebitamt(15000.50);
		a1.setBalance(50000.50);
		
		System.out.println("Credit amount is "+a1.getCreditamt());
		System.out.println("Debit amount is "+a1.getDebitamt());
		System.out.println("Total remaining balance is "+a1.getBalance());
		
	}

}
