package com.day10.collections;

import java.util.ArrayList;
import java.util.List;

public class ArrayListExample {

	public static void main(String[] args) {
		List<String> contacts= new ArrayList<String>();
		
		contacts.add("Arvind");
		contacts.add("Swaraj");
		contacts.add("Rohith");
		contacts.add("Amit");
		contacts.add("Rahul");
		contacts.add("Sumin");
		contacts.add("Rudhay");
		contacts.add("Pradeep");
		contacts.add("Nitin");
		contacts.add("jatin");
		
		System.out.println(contacts.size());
		
		for(int i=0;i<contacts.size();i++)
		{
			System.out.println(contacts.get(i));
		}
		
		contacts.clear();
		
		System.out.println(contacts.get(2));
		
		
		
			
		

	}

}
