package com.day10.collections;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public class ListInterfaceExample {

	public static void main(String[] args) {
		List<Course> list = new LinkedList<Course>();
		list.add(new Course("Java"));
		list.add(new Course("Selenium"));
		list.add(new Course("Protractor"));
		list.add(new Course("Rest Assured"));
		list.add(new Course("Docker"));
		list.add(new Course("AWS"));
		
		List<Course> alist = new ArrayList<Course>();
		alist.add(new Course("Java"));
		alist.add(new Course("Selenium"));
		alist.add(new Course("Protractor"));
		alist.add(new Course("Rest Assured"));
		alist.add(new Course("Docker"));
		alist.add(new Course("AWS"));
		
//		Iterator<Course> contactsIterator = list.iterator();
//		while(contactsIterator.hasNext())
//		{
//			Course s = contactsIterator.next();
//			System.out.println(s.courseName);
//		}
		
		for(int i=0;i<list.size();i++)
		{
			System.out.println(list.get(i));
		}
			
		System.out.println("===========================");
		
		for(int i=0;i<alist.size();i++)
		{
			System.out.println(alist.get(i));
		}
		
	}

}
