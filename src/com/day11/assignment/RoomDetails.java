package com.day11.assignment;

public class RoomDetails implements RoomBillComponent {

	private int billId = 100;
	private String customerName;
	private String typeOfRoom;
	private int noOfExtraPersons;
	private int noOfDaysOfStay;
	private static int counter;

	RoomDetails(String customerName, String typeOfRoom, int noOfExtraPersons, int noOfDaysOfStay) {
		counter = counter + 1;

		this.customerName = customerName;
		this.typeOfRoom = typeOfRoom;
		this.noOfExtraPersons = noOfExtraPersons;
		this.noOfDaysOfStay = noOfDaysOfStay;
	}

	public int getBillId() {
		billId = billId + counter;
		return billId;
	}

	public String getCustomerName() {
		return customerName;
	}

	public String getTypeOfRoom() {
		return typeOfRoom;
	}

	public int getNoOfExtraPersons() {
		return noOfExtraPersons;
	}

	public int getNoOfDaysOfStay() {
		return noOfDaysOfStay;
	}

	public boolean validateNoOfExtraPerson() {
		if (noOfExtraPersons >= 0 && noOfExtraPersons <= 2) {
			return true;
		} else {
			return false;
		}

	}

	public boolean validateTypeOfRoom() {

		if (typeOfRoom.equalsIgnoreCase("standard") || typeOfRoom.equalsIgnoreCase("deluxe")
				|| typeOfRoom.equalsIgnoreCase("cottage")) {
			return true;
		}

		else {
			return false;
		}

	}

	public boolean validateNoOfDaysOfStay() {
		if (noOfDaysOfStay >= 1 && noOfDaysOfStay <= 15) {
			return true;
		} else {
			return false;
		}

	};

	@Override
	public float calculateBill() {
		float baseRoomRate = 0;
		float totalBill = 0;
		if (validateTypeOfRoom() && validateNoOfExtraPerson() && validateNoOfDaysOfStay())
		{
			if (typeOfRoom.equalsIgnoreCase("standard")) {
				baseRoomRate = 2500;
			}

			else if (typeOfRoom.equalsIgnoreCase("deluxe")) {
				baseRoomRate = 3500;
			}

			else if (typeOfRoom.equalsIgnoreCase("cottage")) {
				baseRoomRate = 5500;
			}

		totalBill = (float) ((noOfDaysOfStay * baseRoomRate) + noOfDaysOfStay * (FOOD_CHARGES)
				+ (EXTRA_PERSON_CHARGE * noOfExtraPersons));
		totalBill = (float) ((totalBill * TAX/100) * totalBill);
		return totalBill;

	}
	else
	{
		System.out.println("invalid request");
		return totalBill;
	}

}
}
