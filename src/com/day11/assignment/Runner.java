package com.day11.assignment;

public class Runner {

	public static void main(String[] args) {
		
		RoomDetails arvind = new RoomDetails("arvind", "standard", 2, 1);
		System.out.println("Bill id: "+arvind.getBillId());
		System.out.println("Customer Name: "+arvind.getCustomerName());
		System.out.println("Room Type : "+arvind.getTypeOfRoom());
		System.out.println("No.of Extra Persons: "+arvind.getNoOfExtraPersons());
		System.out.println("No of days of Stay: "+arvind.getNoOfDaysOfStay());
		System.out.println("Total bill is "+arvind.calculateBill());
		
		System.out.println("==============================================");
		
		
		RoomDetails ravi = new RoomDetails("ravi", "deluxe", 1, 5);
		System.out.println("Bill id: "+ravi.getBillId());
		System.out.println("Customer Name: "+ravi.getCustomerName());
		System.out.println("Room Type : "+ravi.getTypeOfRoom());
		System.out.println("No.of Extra Persons: "+ravi.getNoOfExtraPersons());
		System.out.println("No of days of Stay: "+ravi.getNoOfDaysOfStay());
		System.out.println("Total bill is "+ravi.calculateBill());
		
		System.out.println("==============================================");
		
		RoomDetails jatin = new RoomDetails("jatin", "abc", 0, 0);
		System.out.println("Bill id: "+jatin.getBillId());
		System.out.println("Customer Name: "+jatin.getCustomerName());
		System.out.println("Room Type : "+jatin.getTypeOfRoom());
		System.out.println("No.of Extra Persons: "+jatin.getNoOfExtraPersons());
		System.out.println("No of days of Stay: "+jatin.getNoOfDaysOfStay());
		System.out.println("Total bill is "+jatin.calculateBill());
		

	}

}
