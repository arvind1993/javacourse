package com.day7.polymorphism;

public class Runner {

	public static void main(String[] args) {
		Loan accenture = new Loan();
		double corporateEmi;
		corporateEmi = accenture.calculateEMI(5, 10000.50);
		System.out.println("EMI for Corporate Loan is "+corporateEmi);
		
		Loan arvind = new Loan();
		double individualEmi;
		individualEmi = arvind.calculateEMI(10, 50000.50, 7);
		System.out.println("EMI for Individual Personal Loan: "+individualEmi);
        
		Loan jatin = new Loan();
		individualEmi=jatin.calculateEMI(5, 100000.50, 8);
		System.out.println("EMI for Individual Car Loan: "+individualEmi);
		
		Loan ashwin = new Loan();
		individualEmi=ashwin.calculateEMI(15, 1000000.50, 9);
		System.out.println("EMI for Individual Home Loan: "+individualEmi);
	}

}
