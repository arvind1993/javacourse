package com.day7.polymorphism;

public class Loan {
	private double rateOfInterest=8.0;
	
	public double calculateEMI(int tenure, double principal)
	{
		
		double simpleInterest,emi;
		simpleInterest = (principal * tenure * rateOfInterest)/100;
		emi = (simpleInterest + principal)/tenure;
		return emi;
	}
	
	public double calculateEMI(int tenure, double principal, float interestRate)
	{
		double simpleInterest, emi;
		simpleInterest = (principal * tenure * interestRate)/100;
		emi = (simpleInterest + principal)/tenure;
		return emi;
	}
	
	

}
