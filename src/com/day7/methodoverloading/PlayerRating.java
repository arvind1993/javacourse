package com.day7.methodoverloading;

public class PlayerRating {
	private int playerPosition;
	private String playerName;
	private double criticRatingOne,criticRatingTwo,criticRatingThree,averageRating;
	private char category;
	
	public PlayerRating(int playerPosition,String playerName)
	{
		this.playerName=playerName;
		this.playerPosition=playerPosition;
		
	}
	public void calculateAverageRating(int criticRatingOne, int criticRatingTwo)
	{
		this.criticRatingOne=criticRatingOne;
		this.criticRatingTwo=criticRatingTwo;
		this.averageRating = (this.criticRatingOne + this.criticRatingTwo)/2;
	}
	
	public void calculateAverageRating(int criticRatingOne, int criticRatingTwo, int criticRatingThree)
	{
		this.criticRatingOne=criticRatingOne;
		this.criticRatingTwo=criticRatingTwo;
		this.criticRatingThree=criticRatingThree;
		this.averageRating = (this.criticRatingOne + this.criticRatingTwo + this.criticRatingThree)/3;
	}
	
	public void calculateCategory()
	{
		if(averageRating>9)
		{
			category = 'A';
		}
		else if(averageRating>6 && averageRating<=9)
		{
			category = 'B';
		}
		else if(averageRating>0 && averageRating<=6)
		{
			category = 'C';
		}
			
	}
	
	public void display()
	{
		System.out.println("PLAYER NAME: "+playerName);
		System.out.println("PLAYER POSITION: "+playerPosition);
		System.out.println("AVERAGE RATING: "+averageRating);
		System.out.println("CATEGORY: "+category);
	}

}
