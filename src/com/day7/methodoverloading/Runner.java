package com.day7.methodoverloading;

public class Runner {

	public static void main(String[] args) {
	PlayerRating jaspreet = new PlayerRating(4, "Jasprit Bumrah ");
	jaspreet.calculateAverageRating(5,10);
	jaspreet.calculateCategory();
	jaspreet.display();
	
	System.out.println("=====================================");
	
	PlayerRating vinay = new PlayerRating(8, "Vinay");
	vinay.calculateAverageRating(5, 4, 8);
	vinay.calculateCategory();
	vinay.display();
	
	
	

	}

}
