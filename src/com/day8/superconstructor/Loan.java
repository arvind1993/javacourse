package com.day8.superconstructor;

public class Loan {
	int tenure;
	int principal;
	float rateOfInterest;
	
	Loan(int tenure,float rateOfInterest)
	{
		this.tenure=tenure;
		this.rateOfInterest=rateOfInterest;
	}
	double calculateEMI(int principal)
	{
		double si = (principal * rateOfInterest * tenure)/100;
		double emi = (si +principal)/tenure;
		return emi;
	}

}
