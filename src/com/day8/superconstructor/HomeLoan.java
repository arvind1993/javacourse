package com.day8.superconstructor;

public class HomeLoan extends Loan{
	int principal;
	HomeLoan() {
		super(5,5.5f);
	}

	double calculateEMI(int principal) {
		double si = (principal * rateOfInterest * tenure) / 100;
		double emi = (si + principal) / 100;
		return emi;
	}
	
	public void display()
	{
		System.out.println(super.principal);
		System.out.println(super.rateOfInterest);
		System.out.println(super.tenure);
	}

}
