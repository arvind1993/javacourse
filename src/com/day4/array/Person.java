package com.day4.array;

public class Person {
	private static String name;
	private static char gender;
	//private static String[] phoneNumber; //Array declaration can be done in two different types
	private static String phoneNo[];
	
	
	public static void main (String[] args)
	{
	//Array Syntax 1
	name = "Arvind";
	gender = 'M';
	phoneNo = new String[3];
	
	phoneNo[0]="1234567098";
	phoneNo[1]="2334454643";
	phoneNo[2]="9093430889";
	
	System.out.println(name);
	System.out.println(gender);
	System.out.println(phoneNo[2]);
	
	// int = 0;
	//char is blank
	//String is null
	//default val
	
	//Array syntax 2
	int [] primeNo = {1,2,3,5,7};
	System.out.println(primeNo[4]);
	
	
	
	}

}
