package com.day4.array2;

public class EmployeeRecord {

	public static void main(String[] args) {
		double salary[]={23500.0,25080.0,28760.0,22340.0,19890.0};
		//double empSalary[]=new double[5];
		int len=salary.length;
		double sumOfSalary=0;
		int countGreaterThanAverage=0;
		int countLessThanAverage=0;
		//sumOfSalary=salary[0]+salary[1]+salary[2]+salary[3]+salary[4];
		
		for(int i=0;i<len;i++)
		{
			sumOfSalary=sumOfSalary + salary[i];
		}
		System.out.println("Sum of salary is "+sumOfSalary);
		double average=sumOfSalary/len;
		System.out.println("Average of salary is "+average);
		
		/*for(int i=0;i<len;i++)
		{
			empSalary[i]=salary[i];
		} */
		
		for(int i=0;i<len;i++)
		{
			if(salary[i]>average)
			{
				countGreaterThanAverage=countGreaterThanAverage+1;
			}
			else
			{
				countLessThanAverage=countLessThanAverage+1;
			}
		}
		
		System.out.println("Count of Employees having salary greater than average salary is "+countGreaterThanAverage);
		System.out.println("Count of Employees having salary less than average salary is "+countLessThanAverage);
	}

}
