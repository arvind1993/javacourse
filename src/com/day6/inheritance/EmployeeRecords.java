package com.day6.inheritance;

public class EmployeeRecords {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
      PermanentEmployee pe = new PermanentEmployee();
      pe.setEmpid(101);
      pe.setName("Arvind");
      pe.setExp(3);
      pe.setBasicPay(100);
      pe.setHra(500);
      pe.calculateSalary();
      System.out.println("Permanent Employee details are as follows:");
      System.out.println("Employee ID: "+pe.getEmpId());
      System.out.println("Employee Name: "+pe.getName());
      System.out.println("Total experience: "+pe.getExp());
      System.out.println("Basic Pay: "+pe.getBasicPay());
      System.out.println("HRA: "+pe.getHra());
      System.out.println("Total salary: "+pe.getSalary());
      
      ContractEmployee ce = new ContractEmployee();
      ce.setEmpid(201);
      ce.setName("Ashwin");
      ce.setWages(100);
      ce.setHours(8);
      ce.calculateSalary();
      System.out.println("Contract Employee details are as follows:");
      System.out.println("Employee ID: "+ce.getEmpId());
      System.out.println("Employee Name: "+ce.getName());
      System.out.println("Wages: "+ce.getWages());
      System.out.println("Total hours worked: "+ce.getHours());
      System.out.println("Total salary: "+ce.getSalary());
	}

}
