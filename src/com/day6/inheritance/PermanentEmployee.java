package com.day6.inheritance;

//import javax.print.attribute.standard.RequestingUserName;

public class PermanentEmployee extends Employee {
	private double basicPay;
	private double hra;
	private int exp;
	
	public double getBasicPay() {
		return basicPay;
	}
	public void setBasicPay(double basicPay) {
		this.basicPay = basicPay;
	}
	public double getHra() {
		return hra;
	}
	public void setHra(double hra) {
		this.hra = hra;
	}
	public int getExp() {
		return exp;
	}
	public void setExp(int exp) {
		this.exp = exp;
	}
	
	public void calculateSalary(){
		double variableComponent=variablePay();
		double salary = variableComponent+basicPay+hra;
		setSalary(salary);
		System.out.println(salary+"="+variableComponent+"+"+basicPay+"+"+hra);
	}
	
	public double variablePay(){
		double variableComponent = 0;
		
		if(exp<3)
		{
			variableComponent=(basicPay*0/100);
		}
		else if(exp>=3 && exp<5)
		{
			variableComponent=(basicPay*5/100);
		}
		
		else if(exp>=10)
		{
			variableComponent=(basicPay*12/100);
		}
		return variableComponent;	
		}
	
		
}



