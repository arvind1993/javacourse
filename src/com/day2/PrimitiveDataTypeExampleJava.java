package com.day2;

public class PrimitiveDataTypeExampleJava {

	public static void main(String[] args) {
		int studentId=12345;
		boolean isPresent=false;
		double mathScore=33.33;
		char gender='M';
		
		System.out.println("STUDENT ID "+studentId);
		System.out.println("IS PRESENT ? "+isPresent);
		System.out.println("Math Score is "+mathScore);
		System.out.println("GENDER IS "+gender);
		
		

	}

}
