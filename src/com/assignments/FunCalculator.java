package com.assignments;

import java.util.Scanner;

public class FunCalculator {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter number");
		int userNumber=sc.nextInt();
		int originalNumber,remainder,result=0;
		originalNumber = userNumber;
		while(originalNumber!=0)
		{
			remainder = originalNumber % 10;
            result += Math.pow(remainder, 3);
            originalNumber /= 10;
		}
		 if(result == userNumber)
		 {
	            System.out.println(userNumber + " is an Armstrong number.");
		 }
		 else
		 {    
			 System.out.println(userNumber + " is not an Armstrong number.");
		 }

	}

}
