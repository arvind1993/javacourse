package com.assignments;

//import java.util.Scanner;

public class ReverseCalculatorForLoop {

	public static void main(String[] args) {
		//Scanner sc=new Scanner(System.in);
		int a,reverse=0;
		a=10;
		System.out.println("Number to reverse is "+a);
		//a=sc.nextInt();
		
		for(;a!=0;)
		{
			 reverse = reverse * 10;
	         reverse = reverse + a%10;
	         a = a/10;
		}
		 System.out.println("The reverse of a number using for loop is " + reverse);
	      //sc.close();

	}

}
