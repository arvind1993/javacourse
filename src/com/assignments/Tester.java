package com.assignments;

public class Tester {

	public static void main(String[] args) {
		Student s1=new Student();
		Student s2=new Student();
		s1.setStudentId(101);
		s1.setName("Vinay");
		s1.setQualifyingExamMarks(90);
		s1.setResidentStatus('H');
		s1.setYearOfEngg(1);
		
		
		
		System.out.println("Student Name: "+s1.getName());
		System.out.println("Student id: "+s1.getStudentId());
		System.out.println("Qualifying Marks: "+s1.getQualifyingExamMarks());
		System.out.println("Year of Engineering: "+s1.getYearOfEngg());
		System.out.println("Resident Status: "+s1.getResidentStatus());
		
		s2.setStudentId(102);
		s2.setName("Raj");
		s2.setQualifyingExamMarks(65);
		s2.setResidentStatus('D');
		s2.setYearOfEngg(4);
		
		System.out.println("==================================================");
		
		System.out.println("Student Name: "+s2.getName());
		System.out.println("Student id: "+s2.getStudentId());
		System.out.println("Qualifying Marks: "+s2.getQualifyingExamMarks());
		System.out.println("Year of Engineering: "+s2.getYearOfEngg());
		System.out.println("Resident Status: "+s2.getResidentStatus());
		

	}

}
