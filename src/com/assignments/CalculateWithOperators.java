package com.assignments;

public class CalculateWithOperators {

	public static void main(String[] args) {
		int res = 1 + 2; // result is now 3
		System.out.println(res);
		res -= 1; // result is now 2
		System.out.println(res);
		res *= 2; // result is now 4
		System.out.println(res);
		res /= 2; // result is now 2
		System.out.println(res);
		res += 8; 
		System.out.println(res);// result is now 10
		res %= 7; 
		System.out.println(res);// result is now 3
		}

	}


