package com.assignments;

import java.util.Scanner;

public class ReverseCalculator {

	
	public static void main(String[] args) {
	Scanner sc=new Scanner(System.in);
	int a,reverse=0;
	System.out.println("Enter number to reverse");
	a=sc.nextInt();
	
	
	 while(a!= 0)
     {
         reverse = reverse * 10;
         reverse = reverse + a%10;
         a = a/10;
     }
      
     System.out.println("Reverse of the number is " + reverse);
      sc.close();
	}

	
	}
