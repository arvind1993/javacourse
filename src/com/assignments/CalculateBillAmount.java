package com.assignments;

import java.util.Scanner;

public class CalculateBillAmount {
	private static int custId,billId;
	private static double billAmount,finalBillAmount;
	private static int discount;
   

	public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		custId=101;
		billId=1001;
		System.out.println("Enter discount percentage");
		discount=sc.nextInt();
		billAmount=199.99;
		System.out.println("Bill Amount before discount is " +billAmount);
		finalBillAmount=billAmount-(billAmount*discount/100);
		System.out.println("Bill Amount after discount is "+finalBillAmount);
		sc.close();
		
		
	
	}

}
