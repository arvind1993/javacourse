package com.assignments;

public class ReverseCalculatorDoWhileLoop {

	public static void main(String[] args) {
		int a,reverse=0;
		a=12;
		System.out.println("Number to reverse is "+a);
		do
		{
			reverse = reverse * 10;
	         reverse = reverse + a%10;
	         a = a/10;
		}
		while(a!=0);
		System.out.println("The reverse of a number using do while loop is "+reverse );
	}

}
