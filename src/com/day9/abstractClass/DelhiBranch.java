package com.day9.abstractClass;

public class DelhiBranch extends Branch{
	
	@Override
	public boolean validatePhotoProof(String proof) {
		if(proof.equalsIgnoreCase("pan card"))
		{
			return true;
		}
		return false;
	}

	@Override
	public boolean validateAddressProof(String proof) {
		if(proof.equalsIgnoreCase("electricity bill"))
		{
			return true;
		}
		return false;
	}
	

}
